import re
from django.http import HttpResponseRedirect
from django.urls import reverse

class AuthRequired:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Se non sta facendo login/altro con User e non è autenticato, fallo loggare
        if re.search('^/accounts/',request.path)==None and not request.user.is_authenticated:
          return HttpResponseRedirect(reverse('account_login'))
        # altrimenti procedi
        else:
          return self.get_response(request)