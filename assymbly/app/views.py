from django.views.generic       import base, list, detail, edit
from .models                    import *
from django.db.models           import Q
from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls                import reverse
from django.core.exceptions     import PermissionDenied
from datetime                   import datetime, timezone

def userCanReadEvent(user, event):
  return user in event.owners.all() or \
         user in event.invited.all() or \
         event.open

class HomePageView(base.TemplateView):
  template_name = "index.html"

class EventListView(list.ListView):
  template_name       = "event_list.html"
  model               = Event
  context_object_name = "event_list"
  ordering            = ['-start']

  def get_queryset(self):
    user = self.request.user
    # Se supermoderatore legge tutto
    if user.groups.filter(name="Supermoderator").exists():
      return Event.objects.all()
    # Altrimenti legge solo le cose a cui ha accesso
    else:
      return Event.objects.filter(
        Q(open = True) | Q(invited = user) | Q(owners = user)
      )

  def get_context_data(self, **kwargs):
    user = self.request.user
    context = super().get_context_data(**kwargs)
    for i in context['event_list']:
      if i.closed:
        i.color = 'Red'
      elif user in i.owners.all():
        i.color = 'Green'
      elif userCanReadEvent(user, Event.objects.get(pk=i.pk)):
        i.color = 'Yellow'
      else:
        i.color = 'Blue'
    return context

class EventDetailView(UserPassesTestMixin,detail.DetailView):
  template_name       = 'event_view.html'
  model               = Event
  context_object_name = 'event'

  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    context['discussion_list'] = Discussion.objects.filter(event=self.object)
    return context
  
  def test_func(self):
    return ( userCanReadEvent( self.request.user, self.get_object() ) or self.request.user.groups.filter(name="Supermoderator").exists() )

class DiscussionDetailView(UserPassesTestMixin,detail.DetailView):
  template_name       = 'discussion_view.html'
  model               = Discussion
  context_object_name = 'discussion'

  def test_func(self):
    return ( userCanReadEvent( self.request.user, self.get_object().event ) or self.request.user.groups.filter(name="Supermoderator").exists() )
  
  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    context['open_contribution_list']    = Contribution.objects.filter( Q(discussion=self.object) & Q(contribTime__isnull=True) ).order_by('-start','pk')
    context['closed_contribution_list']  = Contribution.objects.filter( Q(discussion=self.object) & Q(contribTime__isnull=False) )
    context['my_open_contribution_list'] = Contribution.objects.filter( Q(discussion=self.object, user=self.request.user) & Q(contribTime__isnull=True) ).order_by('-start','pk')
    context['owner'] = self.request.user in self.object.event.owners.all() or self.request.user.groups.filter(name="Supermoderator").exists()
    return context

class ContributionAddView(UserPassesTestMixin,base.RedirectView):
  obj = None
  def get_object(*args,**kwargs):
    return Discussion.objects.get(pk=kwargs['pk'])
  def test_func(self,*args,**kwargs):
    return userCanReadEvent( self.request.user, self.obj.event )
  def setup(self, request, *args, **kwargs):
    self.obj = self.get_object(*args,**kwargs)
    contrib = Contribution(discussion=self.obj,user=request.user,title=None,start=None,contribTime=None)
    contrib.save()
    return super().setup(request, *args, **kwargs)
  def get_redirect_url(self, *args, **kwargs):
    return reverse('discussion-details',kwargs={'pk':self.obj.pk})

class ContributionDelView(base.RedirectView):
  obj = None
  def setup(self, request, *args, **kwargs):
    contrib = Contribution.objects.get(pk=kwargs['pk'])
    self.obj = contrib.discussion
    if request.user == contrib.user or \
     request.user in self.obj.event.owners.all() or \
     request.user.groups.filter(name="Supermoderator").exists():
      contrib.delete()
    else:
      raise PermissionDenied()
    return super().setup(request, *args, **kwargs)
  def get_redirect_url(self, *args, **kwargs):
    return reverse('discussion-details',kwargs={'pk':self.obj.pk})
  
class ContributionStartView(base.RedirectView):
  obj = None
  def setup(self, request, *args, **kwargs):
    contrib = Contribution.objects.get(pk=kwargs['pk'])
    self.obj = contrib.discussion
    if request.user in self.obj.event.owners.all() or \
     request.user.groups.filter(name="Supermoderator").exists():
      contrib.start = datetime.now()
      contrib.save()
    else:
      raise PermissionDenied()
    return super().setup(request, *args, **kwargs)
  def get_redirect_url(self, *args, **kwargs):
    return reverse('discussion-details',kwargs={'pk':self.obj.pk})
  
class ContributionStopView(base.RedirectView):
  obj = None
  def setup(self, request, *args, **kwargs):
    contrib = Contribution.objects.get(pk=kwargs['pk'])
    self.obj = contrib.discussion
    if request.user in self.obj.event.owners.all() or \
     request.user.groups.filter(name="Supermoderator").exists():
      cusu = contrib.start
      contrib.contribTime = datetime.now(timezone.utc) - contrib.start
      contrib.save()
    else:
      raise PermissionDenied()
    return super().setup(request, *args, **kwargs)
  def get_redirect_url(self, *args, **kwargs):
    return reverse('discussion-details',kwargs={'pk':self.obj.pk})