from django.urls import path, include
from .views import *

urlpatterns = [
    path('',                 HomePageView.as_view() ),
    path('events/',          EventListView.as_view() ),
    path('events/<int:pk>/', EventDetailView.as_view(), name='event-details' ),
    path('discussion/<int:pk>/',        DiscussionDetailView.as_view(),   name='discussion-details'),
    path('discussion/<int:pk>/add',     ContributionAddView.as_view(),    name='contribution-add'),
    path('contribution/<int:pk>/del',   ContributionDelView.as_view(),    name='contribution-del'),
    path('contribution/<int:pk>/start', ContributionStartView.as_view(),  name='contribution-start'),
    path('contribution/<int:pk>/stop',  ContributionStopView.as_view(),   name='contribution-stop'),
]