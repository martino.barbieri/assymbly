from django.db                   import models
from django.contrib.auth.models  import User
from django.contrib              import admin
from django.core.exceptions      import ValidationError


class Event (models.Model):
  title    = models.CharField        ( max_length=200 )
  start    = models.DateTimeField    ()
  closed   = models.BooleanField     ()
  owners   = models.ManyToManyField  ( User, related_name='them_event' )
  invited  = models.ManyToManyField  ( User, related_name='invited_event', blank=True )
  open     = models.BooleanField     ()

class Discussion (models.Model):
  event    = models.ForeignKey       ( Event,      on_delete=models.CASCADE )
  title    = models.CharField        ( max_length=200 )
  start    = models.DateTimeField    ( null=True, blank=True, default=None )
  allowSub = models.BooleanField     ()
  closed   = models.BooleanField     ()
  contribTime = models.DurationField ()

class Contribution (models.Model):
  discussion  = models.ForeignKey    ( Discussion, on_delete=models.CASCADE )
  user     = models.ForeignKey       ( User,       on_delete=models.CASCADE )
  title    = models.CharField        ( max_length=200, null=True, blank=True )
  start    = models.DateTimeField    ( null=True, blank=True, default=None )
  contribTime = models.DurationField ( null=True, blank=True, default=None )

  def clean(self, *args, **kwargs):
    if self.contribTime != None and self.start == None:
      raise ValidationError('Contribution closed but not started.')
    return super().clean(*args, **kwargs) 

  class Meta:
     constraints = [
        models.CheckConstraint(
          check= ~models.Q(contribTime__isnull=False, start__isnull=True),
          name= 'Contribution closed if started.'
        )
     ]

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    pass

@admin.register(Discussion)
class DiscussionAdmin(admin.ModelAdmin):
    pass

@admin.register(Contribution)
class ContributionAdmin(admin.ModelAdmin):
    pass